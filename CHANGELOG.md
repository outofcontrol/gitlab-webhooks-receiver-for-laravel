# Changelog

All notable changes to `tjvb/gitlab-webhooks-receiver-for-laravel` will be documented in this file

## Unreleased

## 1.0.0 - 2021-07-04
- Initial release